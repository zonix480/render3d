import glbs from "./utils/glbs.json" assert { type: "json" };

// Obtener el lienzo de la página
var canvas = document.getElementById("renderCanvas");

// Crear una nueva escena en Babylon.js
var engine = new BABYLON.Engine(canvas, true);
var scene = new BABYLON.Scene(engine);
scene.debugLayer.show();

window.onload = function () {
  // Mostrar el mensaje o el elemento de carga
  const loadingEl = document.getElementById("loading-message");
  if (loadingEl) {
    loadingEl.style.opacity = "0";
    setTimeout(() => {
      loadingEl.style.display = "none";
    }, 1000);
  }
};
//CAMERAS
// Configurar la cámara y la luz
var cameraArcRotateCamera = new BABYLON.ArcRotateCamera(
  "ArcRotateCamera",
  -200,
  -200,
  -80,
  new BABYLON.Vector3(0, 200, 0),
  scene
);
cameraArcRotateCamera.setTarget(BABYLON.Vector3.Zero());
cameraArcRotateCamera.attachControl(canvas, true);
cameraArcRotateCamera.lowerRadiusLimit = 150;
cameraArcRotateCamera.upperRadiusLimit = 300;
cameraArcRotateCamera.wheelDeltaPercentage = 0.01;
cameraArcRotateCamera.applyGravity = true;
scene.collisionsEnabled = true;
cameraArcRotateCamera.checkCollisions = true;
//CameraDrone
const cameraDrone = new BABYLON.FlyCamera(
  "cameraDrone",
  new BABYLON.Vector3(40, 60, 150),
  scene
);
cameraDrone.setTarget(new BABYLON.Vector3(20, 20, 0));
// Airplane like rotation, with faster roll correction and banked-turns.
// Default is 100. A higher number means slower correction.
cameraDrone.rollCorrect = 10;
// Default is false.
cameraDrone.bankedTurn = true;
// Defaults to 90° in radians in how far banking will roll the camera.
cameraDrone.bankedTurnLimit = Math.PI / 2;
// How much of the Yawing (turning) will affect the Rolling (banked-turn.)
// Less than 1 will reduce the Rolling, and more than 1 will increase it.
cameraDrone.bankedTurnMultiplier = 1;
scene.gravity = new BABYLON.Vector3(0, -0.15, 0);

// This attaches the camera to the canvas
cameraDrone.attachControl(canvas, true);
scene.collisionsEnabled = true;
cameraDrone.checkCollisions = true;
//Lights
new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 1, 0), scene);
var gamepadCamera = new BABYLON.GamepadCamera(
  "GamepadCamera",
  new BABYLON.Vector3(40, 60, 150),
  scene
);
gamepadCamera.setTarget(new BABYLON.Vector3(20, 20, 0));
gamepadCamera.attachControl(canvas, true);
gamepadCamera.minZ = 0.45;
//Then apply collisions and gravity to the active camera
gamepadCamera.checkCollisions = true;
gamepadCamera.applyGravity = true;
scene.gravity = new BABYLON.Vector3(0, -0.9, 0);

//Set the ellipsoid around the camera (e.g. your player's size)
gamepadCamera.ellipsoid = new BABYLON.Vector3(0.3, 0.3, 0.3);
scene.activeCamera = cameraArcRotateCamera;

var buttons = document.querySelectorAll("button");
buttons.forEach(function (button) {
  button.addEventListener("click", function () {
    var value = this.dataset.name;
    changeCamera(value);
  });
});
function changeCamera(type) {
  switch (type) {
    case "GamepadCamera":
      scene.activeCamera = gamepadCamera;
      break;
    case "cameraDrone":
      scene.activeCamera = cameraDrone;
      break;
    case "ArcRotateCamera":
      scene.activeCamera = cameraArcRotateCamera;
      break;
    default:
      scente.activeCamera = cameraArcRotateCamera;
      break;
  }
}
loadModelsAndTextures();

var toLoad = glbs.length;

function whenDoneFunction (meshe, glbdata, scene, index){
  toLoad--;
  console.log(index);
  
  let texture = new BABYLON.Texture(glbdata.texture.url, scene);
  texture.vScale = -1;
  let meshMaterial = new BABYLON.StandardMaterial(glbdata.modelName, scene);
  meshMaterial.diffuseTexture = texture;
  meshe.forEach((mesh) => {
    meshe[0].material = meshMaterial;
    meshe[1].material = meshMaterial;
    meshe.checkCollisions = true;
  });
  if (toLoad === 0) {
    alert("loading done!");
  }
};

function loadModelsAndTextures() {
  for (var index = 0; index < glbs.length; index++) {
    let glb = glbs[index];
    // The first parameter can be used to specify which mesh to import. Here we import all meshes
    BABYLON.SceneLoader.ImportMesh(
      "",
      "",
      glbs[index].glbfile,
      scene,
      function (meshes) {
        // Set the target of the camera to the first imported mesh
        //let meshMaterial = new BABYLON.StandardMaterial("test", scene);
        //Texture Type Validation
      
        whenDoneFunction(meshes, glb, scene, index);
        
        //kek = scene.getMeshByName("test");
        //kek.isVisible =false;
      }
    );
  }

  /*const promises = []
  glbs.forEach(glb => {
    //Texture Type Validation
    if (glb.texture?.url) {
      promises.push(loadModel(glb.glbfile, { type: 'file', texture: glb.texture.url }, `${glb.modelName}`));
    }
    if (glb.texture?.color) {
      promises.push(loadModel(glb.glbfile, { type: 'color', texture: glb.texture }, `${glb.modelName}`));
    }
  });
  return Promise.all(promises);*/
}

function loadModel(modelFile, textureModel, materialName) {
  return new Promise((resolve) => {
    BABYLON.SceneLoader.ImportMesh("", "", modelFile, scene, function (meshes) {
      const meshMaterial = new BABYLON.StandardMaterial(materialName, scene);
      //Texture Type Validation
      if (textureModel.type === "file") {
        const texture = new BABYLON.Texture(textureModel.texture, scene);
        texture.vScale = -1;
        meshMaterial.diffuseTexture = texture;
      } else if (textureModel.type === "color") {
        meshMaterial.diffuseColor = new BABYLON.Color3.FromHexString(
          textureModel.texture.color
        );
        meshMaterial.alpha = textureModel.texture.alpha;
      }
      meshes.forEach((mesh) => {
        meshes[0].material = meshMaterial;
        meshes[1].material = meshMaterial;
        mesh.checkCollisions = true;
      });
      resolve();
    });
  });
}

const ground = BABYLON.MeshBuilder.CreateGround("ground", {
  height: 10.5,
  width: 999999999999999999,
  subdivisions: 1,
});
//SKY
const skybox = BABYLON.MeshBuilder.CreateBox("skyBox", { size: 1000.0 }, scene);
const skyboxMaterial = new BABYLON.StandardMaterial("skyBox", scene);
skyboxMaterial.backFaceCulling = false;
skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture(
  "assets/textures/skybox",
  scene
);
skybox.infiniteDistance = true;
skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
skybox.material = skyboxMaterial;

// Iniciar la renderización
engine.runRenderLoop(function () {
  scene.render();
});
