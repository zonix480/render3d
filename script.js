// Obtener el lienzo de la página
var canvas = document.getElementById("renderCanvas");

// Crear una nueva escena en Babylon.js
var engine = new BABYLON.Engine(canvas, true);
var scene = new BABYLON.Scene(engine);
scene.debugLayer.show();
/**
 * Import Mesh Function
 *
 * */
importMesh = function (urlGlb, scene, textureUrl, isAlpha = false, backFaceCulling = false) {
  BABYLON.SceneLoader.ImportMesh("", "", urlGlb, scene, function (meshes) {
    meshMaterial1 = new BABYLON.StandardMaterial("AdoquinOcre", scene);
    let texture = new BABYLON.Texture(textureUrl, scene);
    texture.vScale = -1;
    if(isAlpha){
        texture.hasAlpha = true;
        texture.useAlphaFromDiffuseTexture = true;
      
        meshMaterial1.alpha = 0.36;
    }
    if(backFaceCulling) {
        meshMaterial1.backFaceCulling = false;
    }
    meshMaterial1.diffuseTexture = texture;
    meshes.forEach((ev) => {
      ev.material = meshMaterial1;
      ev.checkCollisions = true;
    });
  });
};

importMesh(
  "assets/_URBANISMO (1).glb",
  scene,
  "textures/_URBANISMO_Corona_Diffuse.webp"
);
importMesh("assets/_TORRE 01Nueva.glb", scene, "textures/_TORRE 01.webp");
importMesh(
  "assets/_VENTANAS_TORRE 01 (1).glb",
  scene,
  "textures/_VENTANAS_TORRE 01_Corona_Diffuse.webp"
);

//TORRE 2
importMesh(
  "assets/_TORRE 02.glb",
  scene,
  "textures/TORRE_02_Corona_Diffuse.webp"
);
importMesh(
  "assets/_COMUNAL.glb",
  scene,
  "textures/_COMUNAL_Corona_Diffuse.webp"
);
importMesh(
  "assets/_CERRAMIENTO URBANISMO.glb",
  scene,
  "textures/_CERRAMIENTO URBANISMO_Corona_Diffuse.webp",
  true,
  true
);
importMesh(
    "assets/_BARANDAS BALCONES_TORRE 01.glb",
    scene,
    "textures/_BARANDAS BALCONES_TORRE 01_Corona_Diffuse.webp",
    true
  );

  importMesh(
    "assets/_BARANDAS BALCONES_TORRE 01.glb",
    scene,
    "textures/_BARANDAS BALCONES_TORRE 01_Corona_Diffuse.webp",
    true
  );

  importMesh(
    "assets/_VENTANAS_TORRE 02.glb",
    scene,
    "textures/VENTANAS_TORRE_02_Corona_Diffuse.webp",
  );

  importMesh(
    "assets/_BARANDAS BALCONES_TORRE 02.glb",
    scene,
    "textures/_BARANDAS BALCONES_TORRE 02_Corona_Alpha.webp",
    true,
    true
  );

  
  var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);
  light.intensity = 2.0;
 

// Configurar la cámara y la luz
var cameraArcRotateCamera = new BABYLON.ArcRotateCamera(
  "ArcRotateCamera",
  39.2484,
  1.4,
  122.0,
  new BABYLON.Vector3(43.16, 16.59, -8.31),
  scene
);

cameraArcRotateCamera.attachControl(canvas, true);
cameraArcRotateCamera.lowerRadiusLimit = 2; //122.0000;
cameraArcRotateCamera.upperRadiusLimit = 400.0;
cameraArcRotateCamera.lowerBetaLimit = 1.0;
cameraArcRotateCamera.upperBetaLimit = 1.4;
cameraArcRotateCamera.wheelDeltaPercentage = 0.01;
cameraArcRotateCamera.applyGravity = true;
scene.collisionsEnabled = true;
cameraArcRotateCamera.checkCollisions = true;

//Plane
function makePlane() {
  const options = {
    width: 3000,
    height: 3000,
    sideOrientation: BABYLON.Mesh.FRONTSIDE,
    updatable: true,
  };
  // const texture = new BABYLON.Color3.FromHexString('#555555');
  const plane = BABYLON.MeshBuilder.CreatePlane("plane", options, scene);
  const texture = new BABYLON.Texture(
    "https://babylongrendering.blob.core.windows.net/textures/Concrete07_GLOSS_6K.jpg",
    scene
  );
  const material = new BABYLON.StandardMaterial("PLANO", scene);
  material.diffuseTexture = texture;
  plane.checkCollisions = true;
  plane.material = material;
  plane.rotation.x = Math.PI / 2;
  plane.position.y = -1;
}
makePlane();

//GUI
var meshT = new BABYLON.Mesh("box", scene);
var advancedTexture =
  BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");

var markerImage = new BABYLON.GUI.Image("marker", "textures/pngegg.png");
markerImage.width = "32px";
markerImage.height = "32px";
markerImage.stretch = BABYLON.GUI.Image.STRETCH_UNIFORM;
markerImage.onPointerUpObservable.add(function () {
  modal.style.display = "block";
});
advancedTexture.addControl(markerImage);
markerImage.linkWithMesh(meshT);
markerImage.billboardMode = BABYLON.AbstractMesh.BILLBOARDMODE_ALL;

meshT.position = new BABYLON.Vector3(53.63, 31.27, -25.36);

var pickCylinder = function (meshEvent) {
  modal.style.display = "block";
};
var light = new BABYLON.HemisphericLight(
  "light",
  new BABYLON.Vector3(0, 1, 0),
  scene
);
// Iniciar la renderización
engine.runRenderLoop(function () {
  scene.render();
});
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[1];

console.log(span);
// When the user clicks on <span> (x), close the modal
span.onclick = function () {
  console.log("acaaa");
  modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};
